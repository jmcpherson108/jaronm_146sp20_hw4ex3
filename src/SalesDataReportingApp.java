
import java.text.NumberFormat;

/**
 * This application displays a four-section report of sales by quarter for a 
 * company with four sales regions (Region 1, Region 2,
 * Region 3, and Region 4).
 * @author JaRon
 * @version CSCI 146 Spring 2020 HW4 Exercise 3
 */
public class SalesDataReportingApp {
    
    public static void main(String[] args) {
        
        NumberFormat currency = NumberFormat.getCurrencyInstance();
        
        /*
        The quarterly sales for each region should be hard-coded into the program using the numbers shown in the sample console output
        (see next page). The sales numbers should be stored in a rectangular (2-D) array.
        */
        double[][] quarterlySales = new double[4][4];
        quarterlySales[0][0]= 1540.00;
        quarterlySales[1][0]= 1130.00;
        quarterlySales[2][0]= 1580.00;
        quarterlySales[3][0]= 1105.00;
        quarterlySales[0][1]= 2010.00;
        quarterlySales[1][1]= 1168.00;
        quarterlySales[2][1]= 2305.00;
        quarterlySales[3][1]= 4102.00;
        quarterlySales[0][2]= 2450.00;
        quarterlySales[1][2]= 1847.00;
        quarterlySales[2][2]= 2710.00;
        quarterlySales[3][2]= 2391.00;
        quarterlySales[0][3]= 1845.00;
        quarterlySales[1][3]= 1491.00;
        quarterlySales[2][3]= 1284.00;
        quarterlySales[3][3]= 1576.00;
        
        /* 
        * Operation Requirement 2:
        * The first section of the report lists the sales by quarter 
        * for each region.
        */
        System.out.println("Region\tQ1\t\tQ2\t\tQ3\t\tQ4");
        
        /*
        * Specification 3: 
        * The first section of the report should use nested for loops to display 
        * the sales by quarter for each region. Use tabs to line up the
        * columns for this section of the report.
        */
        for( int region = 0; region < 4; region++ )
        {
            System.out.print(currency.format(quarterlySales) + "\t");
        } // end for

        /* Operational Requirement 3:
         * The second section summarizes the total annual sales by region.
         */
        
        /* Specification 4:
        * The second section of the report should use nested for loops to calculate 
        * the sales by region by adding up the quarterly sales for
        * each region.
        */

        for(int i =0; i < quarterlySales.length; i++){
                    double regionSales1 = quarterlySales[0][0] + quarterlySales[1][0] + 
                quarterlySales[2][0] + quarterlySales[3][0];
        
        double regionSales2 = quarterlySales[0][1] + quarterlySales[1][1] +
                quarterlySales[2][1] + quarterlySales[3][1];
        
        double regionSales3 = quarterlySales[0][2] + quarterlySales[1][2] +
                quarterlySales[2][2] + quarterlySales[3][2];
        
        double regionSales4 = quarterlySales[0][3] + quarterlySales[1][3] +
                quarterlySales[2][3] + quarterlySales[3][3];
        System.out.println(currency.format("Sales by region: "));
        System.out.println(currency.format("Region 1: "  + regionSales1));
        System.out.println(currency.format("Region 2: "  + regionSales2));
        System.out.println(currency.format("Region 3: "  + regionSales3));
        System.out.println(currency.format("Region 4: "  + regionSales4));
            } // end for 
        
        

        /*
        * Specification 5:
        * The third section of the report should use nested for loops to calculate 
        * the sales by quarter by adding up the individual region
        * sales for each quarter.
        */
        
        System.out.println(currency.format("Sales by quarter: "));
        
        /*
        * Specification 6:
        * The fourth section of the report should use an enhanced for loop to 
        * calculate the total annual sales for all regions.
        */
        
        System.out.println(currency.format("Total annual sales, all regions: " ));
        
        /*
        * Specification 7:
        * Use the NumberFormat class to format the sales numbers using the 
        * U.S. currency format.
        */
    } // end method main
    
} // end class SalesDataReportingApp
